import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;

/**
 * Initiates the IRC connection.
 * PircBot documentation can be found here - http://www.jibble.org/javadocs/pircbot/index.html
 * @author Stephen
 */
public class IrcHandler extends PircBot {
	
	/**
	 * The server to connect to.
	 */
	private final static String HOST = "play.ubernation.org";
	
	/**
	 * The channel to connect to.
	 */
	private final static String CHANNEL = "#Exorth";

	/**
	 * The player.
	 */
	public RSClient client;
	
	/**
	 * The sender from IRC.
	 */
	private String ircSender;
	
	/**
	 * Sends a private message in the form of 'From + sender'.
	 */
	private final int PRIVATE_MESSAGE = 3;
	
	/**
	 * Displays message on the IRC tab.
	 */
	private final int IRC_MESSAGE = 9;
	
	
	/**
	 * Establishes the IRC connection.
	 * @param userName The player's user name.
	 * @throws Exception Failure to connect.
	 */
	public void startConnection(String userName) throws Exception {
		this.setName(userName);
        this.setVerbose(false);
		this.connect(HOST);
	    this.joinChannel(CHANNEL);
	}
	
	public void onMessage(String channel, String sender, String login, String hostname, String message) {
		
		ircSender = sender;		
		/**
		 * From IRC to game.
		 */
		client.pushMessage(message, IRC_MESSAGE, sender);
	}
	
	/**
	 * Sends a message from the game to the IRC.
	 * @param message The message to be sent.
	 */
	public void sendGameMessage(String message) {
		if (client.ircVisible == true) {
			sendMessage("#Exorth", message);
			client.pushMessage(message, 9, client.myUsername);
		} else {
			client.pushMessage("Please switch the IRC on before talking in it.", 0, "");
		}
		
		if (message.equalsIgnoreCase("!users")) {
			ircSender = "Exorth";
			client.pushMessage("There are currently " + getUsers() + " users in the IRC.", PRIVATE_MESSAGE, ircSender);
		}
	}
	
	/**
	 * Gets the amount of users currently active in IRC.
	 * @return The amount of users.
	 */
	private int getUsers() {
		User[] users = getUsers(CHANNEL);
		int userCount = 0;
		for (User user : users) {
			userCount++;
		}
		return userCount;
	}
	
	/**
	 * Gets the ircSender.
	 * @return The ircSender.
	 */
	public String getIRCSender() {
		return ircSender;
	}
}