import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * A developer console.
 * @author Stephen
 */
public class DeveloperConsole {

	/**
	 * Date format for more accurate logs.
	 */
	private static DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	
	/**
	 * The fonts for the console.
	 */
	private RSFont newRegularFont, newBoldFont;
	
	ArrayList<String> consoleMessages;
	
	/**
	 * The color of the console.
	 */
	private int color;
	
	/**
	 * Whether or not the console is currently open.
	 */
	private boolean consoleOpen;
	
	/**
	 * Constructs a developer console.
	 * @param color The color of the console.
	 */
	public DeveloperConsole(RSFont newRegularFont, RSFont newBoldFont, int color) {
		this.newRegularFont = newRegularFont;
		this.newBoldFont = newBoldFont;
		consoleMessages = new ArrayList<String>();
		this.color = color;
		consoleOpen = false;
	}
	
	/**
	 * Draws the console on the game screen.
	 */
	public void drawConsole(int consoleWidth) {
		if (!consoleOpen) return;
		
		RSDrawingArea.transparentBox(334, 0, 0, color, consoleWidth, 0, 90);
		RSRaster.drawPixels(1, 315, 0, 0xFFFFFF, consoleWidth);
		newBoldFont.drawBasicString("-->", 3, 328, 0xFFFFFF, 0);
	}
	
	public void drawConsoleText() {
		if (!consoleOpen) return;
		
		int yPos = 308;
		for (String message : consoleMessages) {
			newRegularFont.drawBasicString(message, 9, yPos, 0xFFFFFF, 0);
			yPos -= 18;
		}
    }
	
	public void sendConsoleMessage(String message) {
		consoleMessages.add(getTimeStamp() + message);
	}
	
	/**
	 * Get the current time of the message.
	 * @return The time.
	 */
	protected static String getTimeStamp() {
		Date date = new Date();
		return dateFormat.format(date) + ": ";
	}
	
	public ArrayList<String> getConsoleMessages() {
		return consoleMessages;
	}
	
	/**
	 * Sets the consoleOpen boolean.
	 * @param flag The state to set it to.
	 */
	public void setConsoleOpen(boolean flag) {
		consoleOpen = flag;
	}
	
	public boolean isOpen() {
		return consoleOpen;
	}
}
